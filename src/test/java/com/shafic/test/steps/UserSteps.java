package com.shafic.test.steps;

import com.shafic.test.pages.CirclesSignInPage;
import com.shafic.test.pages.FacebookLoginPage;
import com.shafic.test.pages.FacebookWallPage;
import net.thucydides.core.annotations.Step;

import java.util.HashMap;

public class UserSteps {

    CirclesSignInPage circlesSignInPage;
    FacebookLoginPage facebookLoginPage;
    FacebookWallPage facebookWallPage;


    @Step
    public void opens_circles_page() {
        circlesSignInPage.open();
    }

    @Step
    public void opens_facebook_web_page() {
        facebookLoginPage.open();
      }

    @Step
    public void login_to_cireclesPage(HashMap map) {
        circlesSignInPage.fillAndSubmit(map);
    }

    @Step
    public void login_to_Facebook_WebPage(HashMap map) {
        facebookLoginPage.fillAndSubmit(map);
    }

    @Step
    public void Facebook_WebPage_navigate_newsFeed() {
        facebookWallPage.clickNewsFeedLink();
    }

    @Step
    public void Facebook_WebPage_ComposePost() {
        facebookWallPage.clickComposePostLink();
    }

}
