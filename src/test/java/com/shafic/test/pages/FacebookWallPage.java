package com.shafic.test.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.util.Map;

public class FacebookWallPage extends PageObject {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "a[title='News Feed']")
    @CacheLookup
    private WebElement newsFeed;


    @FindBy(xpath = "//span[text()='Compose Post']")
    @CacheLookup
    private WebElement composePost;


    @FindBy(xpath = "//div[@role='textbox']")
    private WebElement postMsg;

    @FindBy(xpath = "//div[@data-offset-key='dsu5-0-0']")
    private WebElement postMsgBox;


    @FindBy(xpath = "//button/span[text()='Post']")
    private WebElement post;

    /**
     * Click on Compose Post Link.
     *
     * @return the FacebookWallPage class instance.
     */
    public FacebookWallPage clickComposePostLink() {

        composePost.click();
        try {
            Thread.sleep(4000);
        }catch (Exception e){
            System.out.println(e);
        }

        postMsg.sendKeys("Hello Hi!!!");
        post.click();
        return this;
    }

    /**
     * Click on News Feed Link.
     *
     * @return the FacebookWallPage class instance.
     */
    public FacebookWallPage clickNewsFeedLink() {
        newsFeed.click();
        return this;
    }

    public FacebookWallPage() {
    }

    public FacebookWallPage(WebDriver driver) {
        this();
        this.driver = driver;
    }





}
