package com.shafic.test.pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.Map;

@DefaultUrl("https://shop.circles.life/login")
public class CirclesSignInPage extends PageObject {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;


    @FindBy(name = "email")
    @CacheLookup
    private WebElement EmailAddress;

    @FindBy(name = "password")
    @CacheLookup
    private WebElement Password;


    @FindBy(xpath = "(//button[@type='button'])[2]")
    @CacheLookup
    private WebElement SignIn;


    public CirclesSignInPage() {
    }

    public CirclesSignInPage(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public CirclesSignInPage(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public CirclesSignInPage(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }



    /**
     * Fill every fields in the page.
     *
     * @return the  class instance.
     */
    public CirclesSignInPage fill(HashMap map) {
        setEmailAddress(map.get("email").toString());
        setPasswordField(map.get("password").toString());
        return this;
    }

    /**
     * Click on Log In 
     *
     * @return the class instance.
     */
    public CirclesSignInPage submit() {
        SignIn.click();
        return this;
    }


    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the CirclesSignInPage class instance.
     */
    public CirclesSignInPage fillAndSubmit(HashMap map) {
        fill(map);
        return submit();
    }



    /**
     * Set value to Email Address Email Address Sign In Password field.
     *
     * @return the CirclesSignInPage class instance.
     */
    public CirclesSignInPage setPasswordField(String password) {
        Password.sendKeys(password);
        return this;
    }

    /**
     * Set value to Email Address Email Address Sign In Password field.
     *
     * @return the CirclesSignInPage class instance.
     */
    public CirclesSignInPage setEmailAddress(String emailAddress) {
        EmailAddress.sendKeys(emailAddress);
        return this;
    }

    public CirclesSignInPage verifyAccountDetails(String accountDetails) {
        //accountName.getText().contains(accountDetails);
        return this;
    }
}
